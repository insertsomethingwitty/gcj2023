;;;; ASCII Art

(defun solve (&optional (in *standard-input*))
  (dotimes (caseno (the (integer 0 1000) (read in)))
    (format t "Case #~D: " (+ caseno 1))
    (solve-case in)))

(defun solve-case (in)
  (let ((n (read in)))
    (format t "~D~%" (char-at (1- n)))))

(defun char-at (n)
  (let ((iteration (floor n 26)))
    (let ((gen (generation iteration)))
      (let ((start (/ (* gen (1+ gen)) 2)))
        (let ((offset (- n (* start 26))))
          ;; (warn "gen ~D start ~D offset ~D" gen start offset)
          (code-char (+ (char-code #\A)
                        (floor offset (+ gen 1)))))))))

(defun generation (n)
  (generation-1 n (floor (sqrt (* 2 (sqrt (+ (* n n) n)))))))

(defun generation-1 (n est)
  (let ((test (/ (* est (1+ est)) 2)))
    (cond ((> test n)
           (generation-1 n (1- est)))
          ((< test (- n est))
           (generation-1 n (1+ est)))
          (t est))))

(solve)
