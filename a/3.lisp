;;;; Rainbow Sort

(defun solve (&optional (in *standard-input*))
  (dotimes (caseno (the (integer 0 1000) (read in)))
    (format t "Case #~D: " (+ caseno 1))
    (solve-case in)))

(defun solve-case (in)
  (let ((n (read in)))
    (let ((cards '()))
      (dotimes (k n)
        (push (read in) cards))
      (let ((s (rainbow (nreverse cards))))
        (if s
            (format t "~{~D~^ ~}~%" s)
            (format t "IMPOSSIBLE~%"))))))

(defun rainbow (cards)
  (let ((seen (make-hash-table :test #'eql :size (length cards))))
    (rainbow-1 cards seen '())))

(defun rainbow-1 (cards seen result)
  (cond ((endp cards) (nreverse result))
        ((and result (= (first cards) (first result)))
         (rainbow-1 (rest cards) seen result))
        ((gethash (first cards) seen)
         nil)
        (t (setf (gethash (first cards) seen) 1)
           (rainbow-1 (rest cards) seen (cons (first cards) result)))))

(solve)
