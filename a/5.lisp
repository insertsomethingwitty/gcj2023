;;;; Untie

(defun solve (&optional (in *standard-input*))
  (dotimes (caseno (the (integer 0 1000) (read in)))
    (format t "Case #~D: " (+ caseno 1))
    (solve-case in)))

(defun solve-case (in)
  (let ((s (read-line in)))
    (format t "~D~%" (untie s))))

(defun untie (s)
  (let ((start (find-first-change s)))
    (if start
        (untie-1 (concatenate 'string (subseq s start) (subseq s 0 start)) 0 0)
        (ceiling (length s) 2))))

(defun untie-1 (s pos n)
  (if (= pos (length s))
      n
      (let ((next (find-first-change s :start pos)))
        (cond ((not next)
               (+ n (floor (- (length s) pos) 2)))
              ((= next (+ 1 pos))
               (untie-1 s (1+ pos) n))
              (t (untie-1 s next (+ n (floor (- next pos) 2))))))))

(defun find-first-change (s &key (start 0))
  (position (char s start) s :start start :test-not #'char=))

(solve)
