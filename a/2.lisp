;;;; Illumination Optimization

(defun solve (&optional (in *standard-input*))
  (dotimes (caseno (the (integer 0 1000) (read in)))
    (format t "Case #~D: " (+ caseno 1))
    (solve-case in)))

(defun solve-case (in)
  (let* ((m (read in))
         (r (read in))
         (n (read in)))
    (let ((x (make-array (list n))))
      (dotimes (k n)
        (setf (aref x k) (read in)))
      (let ((solution (solve-case-1 m r x)))
        (if solution
            (format t "~D~%" solution)
            (format t "IMPOSSIBLE~%"))))))

(defun solve-case-1 (m r x)
  (let ((a (solve-case-2 m r x 0 0 0))
        (b (let ((rev (reverse x)))
             (dotimes (k (length rev))
               (setf (aref rev k)
                     (- m (aref rev k))))
             (solve-case-2 m r rev 0 0 0))))
    (if (and a b) (min a b)
        (or a b))))

(defun solve-case-2 (m r x last-light k nbulbs)
  (cond ((>= last-light m) nbulbs)
        ((= k (length x)) nil)
        ((> (aref x k) (+ last-light r)) nil)
        ((and (< (+ k 1) (length x))
              (<= (aref x (+ k 1)) (+ last-light r)))
         (solve-case-2 m r x last-light (+ 1 k) nbulbs))
        (t
         (solve-case-2 m r x (+ (aref x k) r) (+ 1 k) (+ 1 nbulbs)))))

(solve)
